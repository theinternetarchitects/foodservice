package foodservice

import (
	"encoding/json"
	"net/http"
	"os"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/options"
	"gitlab.com/theinternetarchitects/foodservice/models"
	"gitlab.com/theinternetarchitects/neo4jconnector"
)

// FoodService ...
type FoodService struct {
	URI         string
	Client      *http.Client
	MongoClient *mongo.Client
	Neo4jClient *neo4jconnector.Neo4jConnector
}

// New Create a new instance of FoodService
func New() *FoodService {
	mongoClient, _ := mongo.NewClient(options.Client().ConnString.String("mongodb://" + os.Getenv("FOOD_DB")))
	neo4jClient, _ := neo4jconnector.New("something", "username", "password")
	return &FoodService{
		URI: os.Getenv("FOOD_SERVICE_URI"),
		Client: &http.Client{
			Timeout: time.Second * 5,
		},
		MongoClient: mongoClient,
		Neo4jClient: neo4jClient,
	}
}

// GetFood ...
func (s *FoodService) GetFood(barcode string) (*models.FoodModel, interface{}) {
	resp, err := s.Client.Get(s.URI + "/product/" + barcode)

	if err != nil {
		return nil, 500
	}

	if resp.StatusCode != 200 {
		return nil, resp.StatusCode
	}

	var decodedResp map[string]interface{}

	err = json.NewDecoder(resp.Body).Decode(&decodedResp)

	if err != nil {
		return nil, 500
	}

	return &models.FoodModel{
		Barcode:                 decodedResp["product"].(map[string]interface{})["id"].(string),
		BrandName:               decodedResp["product"].(map[string]interface{})["brands"].(string),
		ProductImage:            decodedResp["product"].(map[string]interface{})["image_url"].(string),
		ProductIngredientsImage: decodedResp["product"].(map[string]interface{})["image_ingredients_url"].(string),
		ProductName:             decodedResp["product"].(map[string]interface{})["product_name"].(string),
	}, nil
}

// GetFoodsByCategory ...
func (s *FoodService) GetFoodsByCategory() ([]models.FoodModel, interface{}) {
	s.MongoClient.Connect()
	return []models.FoodModel{}, nil
}
