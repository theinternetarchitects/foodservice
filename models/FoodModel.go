package models

// FoodModel The data represention of a food
type FoodModel struct {
	ProductName             string `json:"productName"`
	BrandName               string `json:"brandName"`
	Barcode                 string `json:"barcode"`
	ProductImage            string `json:"productImage"`
	ProductIngredientsImage string `json:"productIngredientsImage"`
}
